package Timer_A1;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;




public class Main {

	public static void main(String[] args) {
		
		Random r = new Random();
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		System.out.println("Inicio el main.");

		
		for(int i=0;i<10;i++)
			executor.execute(new MyThread(r.nextInt(9)+1));	
		
		executor.shutdown();
		
		try {
			executor.awaitTermination(10, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
		
		
		System.out.println("Acabo el Main				| Timer.Main --> linea 38");
	}

}
